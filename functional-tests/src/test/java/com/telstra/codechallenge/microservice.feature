# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As an api user I want to test spring boot microservice 

  Scenario: Is the health uri available and status=UP
    Given url microserviceUrl
    And path '/actuator/health'
    When method GET
    Then status 200
    And match response == { "status" : "UP" }

  Scenario: Is the info uri available and returning data
    Given url microserviceUrl
    And path '/actuator/info'
    When method GET
    Then status 200
    And match response ==
      """
        {
          build: {
            version: '#string',
            artifact: '#string',
            name: '#string',
            group: '#string',
            time: '#string'
          }
        }
      """
    
    Scenario: Search default 1 oldest users with zero followers
    Given url microserviceUrl
    And path '/search/oldest/users'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
	And match response ==  [{  html_url : '#string', login : '#string', id : '#string'  }]
    
    Scenario: Search default 3 oldest users with zero followers
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'3'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { html_url : '#string', login : '#string', id : '#string' }
    And match response ==  '#[3] quoteSchema'
    
    Scenario: Search zero oldest users with zero followers
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'0'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    And match response == 
    """
    []
    """
    
    Scenario: Search 56 oldest users with zero followers
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'56'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { html_url : '#string', login : '#string', id : '#string' }
    And match response ==  '#[56] quoteSchema'
    
    Scenario: Search 999 oldest users with zero followers for load testing
    * configure readTimeout = 100000
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'999'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { html_url : '#string', login : '#string', id : '#string' }
    And match response ==  '#[999] quoteSchema'
    
    Scenario: Search max oldest users with zero followers for testing maximum data limit in git which is 1000
    * configure readTimeout = 100000
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'9999999'}
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { html_url : '#string', login : '#string', id : '#string' }
    And match response ==  '#[1000] quoteSchema'
    
    Scenario: Search oldest user with zero followers using bad input
    Given url microserviceUrl
    And path '/search/oldest/users'
    And params {'count':'abc'}
    When method GET
    Then status 400
    And match header Content-Type contains 'application/json'
    * def quoteSchema = { html_url : '#string', login : '#string', id : '#string' }
    And match response !=  'quoteSchema'
    
    Scenario: Search n oldest users with zero followers with invalid url
    Given url microserviceUrl
    And path '/search/oldest/users/abc'
    When method GET
    Then status 404