package com.telstra.codechallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
//import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.HystrixCommandProperties;

//@EnableHystrixDashboard
@EnableCircuitBreaker
@SpringBootApplication
public class MicroServiceMain {

	public static void main(String[] args) {
		HystrixCommandProperties.Setter().withExecutionTimeoutInMilliseconds(100000);
		SpringApplication.run(MicroServiceMain.class, args);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

}
