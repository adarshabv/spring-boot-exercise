package com.telstra.codechallenge.retrieveusers.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.telstra.codechallenge.retrieveusers.service.RetrieveOldUsersService;

@RestController
public class RetrieveOldUsersController {

	private static final Logger logger = LogManager.getLogger(RetrieveOldUsersController.class);

	@Autowired
	RetrieveOldUsersService retrieveOldUsersService;

	@RequestMapping(value = "${retrieve-oldest-users}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String retrieveOldestUsers(
			@RequestParam(required = true, value = "count", defaultValue = "1") int inputCount) {

		logger.info("Request received in retrieveOldestUsers().....");
		return retrieveOldUsersService.retrieveNOldestUsers(inputCount);
	}
}
