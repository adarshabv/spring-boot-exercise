package com.telstra.codechallenge.retrieveusers.service;

import org.springframework.stereotype.Service;

@Service
public interface RetrieveOldUsersService {

	public String retrieveNOldestUsers(int count);

}
