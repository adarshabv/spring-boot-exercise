package com.telstra.codechallenge.retrieveusers.service;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.telstra.codechallenge.retrieveusers.util.RetrieveOldestUsersConstants;
import com.telstra.codechallenge.retrieveusers.vo.GitHubUser;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

@Service
public class RetrieveOldUsersServiceImpl implements RetrieveOldUsersService {

	private static final Logger logger = LogManager.getLogger(RetrieveOldUsersServiceImpl.class);

	@Autowired
	private RestTemplate restTemplate;

	@Value("${api.github.prefix}")
	private String gitHubApiPrefix;

	@Value("${api.github.search.user.endpoint}")
	private String gitHubApiEndpoint;

	@Value("${api.github.param.qualifier.value}")
	private String githubqualifierValue;

	@Value("${api.github.param.sort.value}")
	private String githubSortValue;

	@Value("${api.github.param.order.value}")
	private String githubOrdervalue;

	@HystrixCommand(fallbackMethod = "callGitServiceAndGetData_Fallback",commandProperties= {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "20000") })
	@Override
	public String retrieveNOldestUsers(int count) {
		logger.debug("starting retrieveNOldestUsers in retrieveOldestUsersServiceImpl()..");
		HttpHeaders header = new HttpHeaders();
		header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
		header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
		ObjectMapper objectMapper = new ObjectMapper();
		GitHubUser[][] mainUserArray = new GitHubUser[1][0];
		retrieveUsersFromGit(count, objectMapper, requestEntity, mainUserArray);
		List<GitHubUser> userListNew = new ArrayList<GitHubUser>();
		List<GitHubUser> userList = Arrays.asList(mainUserArray[0]);
		for (int i = 0; i < count; i++) {
			try {
				userListNew.add(userList.get(i));
			} catch (ArrayIndexOutOfBoundsException e) {
				logger.info("breaking for loop due to ArrayIndexOutOfBoundsException in"
						+ " retrieveOldestUsersServiceImpl()..");
				break;
			}
		}
		String strReturn = "";
		try {
			strReturn = objectMapper.writeValueAsString(userListNew);
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException " + e);
		}
		logger.debug("ending retrieveNOldestUsers in retrieveOldestUsersServiceImpl()..");
		return strReturn;
	}

	private void retrieveUsersFromGit(int count, ObjectMapper objectMapper, HttpEntity<HttpHeaders> requestEntity,
			GitHubUser[][] mainUserArray) {
		logger.debug("starting retrieveUsersFromGit in retrieveOldestUsersServiceImpl()..");
		int maxItemsPerPage = 100;
		int countRemainder = count % maxItemsPerPage;
		int countQuotient = count / maxItemsPerPage;
		for (int i = 1; i <= countQuotient + 1; i++) {
			URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(gitHubApiEndpoint)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_QUALIFIER, githubqualifierValue)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_SORT, githubSortValue)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_ORDER, githubOrdervalue)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PAGE, i)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PERPAGE,
							i == countQuotient + 1 ? countRemainder : maxItemsPerPage)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CID,
							RetrieveOldestUsersConstants.GITHUBAPIPARAM_CIDV)
					.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CS,
							RetrieveOldestUsersConstants.GITHUBAPIPARAM_CSV)
					.build().encode().toUri();
			logger.info("GitHub API call to retrieve Oldest Users..");

			String githubJsonResponse = "[{}]";
			try {
				ResponseEntity<JsonNode> searchUserResponse = restTemplate.exchange(gitAPIUrl, HttpMethod.GET,
						requestEntity, JsonNode.class);
				logger.info("Received response " + searchUserResponse.getBody().get("items") + "from github");
				githubJsonResponse = searchUserResponse.getBody().get("items").toString();
			} catch (RestClientException e) {
				logger.error("RestClientException " + e);
				break;
			}
			GitHubUser[] userArrayTemp = new GitHubUser[0];
			try {
				userArrayTemp = objectMapper.readValue(githubJsonResponse, GitHubUser[].class);
			} catch (JsonMappingException ex) {
				logger.error("JsonMappingException " + ex);
			} catch (IOException ex) {
				logger.error("JsonProcessingException " + ex);
			}
			mainUserArray[0] = (GitHubUser[]) ArrayUtils.addAll(mainUserArray[0], userArrayTemp);
		}
		logger.debug("ending retrieveUsersFromGit in retrieveOldestUsersServiceImpl()..");
	}

	@SuppressWarnings("unused")
	private String callGitServiceAndGetData_Fallback(int count) {
		logger.error("Github Service is down!!! fallback route enabled...");
		return "CIRCUIT BREAKER ENABLED!!! No Response From Github Service at this moment. "
				+ " Service will be back shortly - " + new Date();
	}

}
