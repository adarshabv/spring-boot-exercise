package com.telstra.codechallenge.retrieveusers.util;

public class RetrieveOldestUsersConstants {

	public static final String GITHUBAPIPARAM_SORT = "sort";
	public static final String GITHUBAPIPARAM_ORDER = "order";
	public static final String GITHUBAPIPARAM_QUALIFIER = "q";
	public static final String GITHUBAPIPARAM_PAGE = "page";
	public static final String GITHUBAPIPARAM_PERPAGE = "per_page";
	public static final String GITHUBAPIPARAM_CID = "client_id";
	public static final String GITHUBAPIPARAM_CIDV = "ffefebcaa87016237333";
	public static final String GITHUBAPIPARAM_CS = "client_secret";
	public static final String GITHUBAPIPARAM_CSV = "d1bf376dbf7b5ee63af8e926b333e5f54af0dbab";

}
