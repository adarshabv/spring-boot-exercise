package com.telstra.codechallenge.retrieveusers.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GitHubUser {
	@JsonProperty("id")
	private String id;

	@JsonProperty("login")
	private String login;

	@JsonProperty("html_url")
	private String html_url;
}
