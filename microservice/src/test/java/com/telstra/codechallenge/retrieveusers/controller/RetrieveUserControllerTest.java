package com.telstra.codechallenge.retrieveusers.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;

import com.telstra.codechallenge.retrieveusers.service.RetrieveOldUsersService;
import com.telstra.codechallenge.retrieveusers.util.RetrieveUsersUtil;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RetrieveUserControllerTest {

	@LocalServerPort
	private int port;

	@Mock
	private RetrieveOldUsersService retrieveOldUsersService;

	@InjectMocks
	private RetrieveOldUsersController retrieveOldUsersController = new RetrieveOldUsersController();

	@Test
	public void getOldestUsersTest() throws Exception {
		String mockedResponse = RetrieveUsersUtil.getUserDetails();
		when(retrieveOldUsersController.retrieveOldestUsers(1)).thenReturn(mockedResponse);
		String response = retrieveOldUsersController.retrieveOldestUsers(1);
		assertEquals(mockedResponse, response);
	}

}
