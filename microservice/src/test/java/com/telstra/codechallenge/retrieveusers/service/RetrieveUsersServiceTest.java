package com.telstra.codechallenge.retrieveusers.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.net.URI;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.databind.JsonNode;
import com.telstra.codechallenge.MicroServiceMain;
import com.telstra.codechallenge.retrieveusers.util.RetrieveOldestUsersConstants;
import com.telstra.codechallenge.retrieveusers.util.RetrieveUsersUtil;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class RetrieveUsersServiceTest {

	@LocalServerPort
	private int port;

	@Value("${api.github.prefix}")
	private String gitHubApiPrefix;

	@Value("${api.github.search.user.endpoint}")
	private String gitHubApiEndpoint;

	@Value("${api.github.param.qualifier.value}")
	private String githubqualifierValue;

	@Value("${api.github.param.sort.value}")
	private String githubSortValue;

	@Value("${api.github.param.order.value}")
	private String githubOrdervalue;

	@Mock
	private RestTemplate restTemplate;

	@Mock
	HttpEntity<HttpHeaders> requestEntity;

	@InjectMocks
	private RetrieveOldUsersService retrieveUserServiceImpl = new RetrieveOldUsersServiceImpl();

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		ReflectionTestUtils.setField(retrieveUserServiceImpl, "gitHubApiPrefix", "https://api.github.com");
		ReflectionTestUtils.setField(retrieveUserServiceImpl, "gitHubApiEndpoint", "/search/users");
		ReflectionTestUtils.setField(retrieveUserServiceImpl, "githubqualifierValue", "followers:0");
		ReflectionTestUtils.setField(retrieveUserServiceImpl, "githubSortValue", "joined");
		ReflectionTestUtils.setField(retrieveUserServiceImpl, "githubOrdervalue", "asc");

		ResponseEntity<JsonNode> mockedResponse = RetrieveUsersUtil.getUserDetailsJSON();
		URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(gitHubApiEndpoint)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_QUALIFIER, githubqualifierValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_SORT, githubSortValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_ORDER, githubOrdervalue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PERPAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CID,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CIDV)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CS,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CSV)
				.build().encode().toUri();
		HttpHeaders header = new HttpHeaders();
		header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
		header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
		when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
				.thenReturn(mockedResponse);
	}

	@Test
	public void getOldestUsersTest() throws Exception {
		String mockedResponseBodyStr = RetrieveUsersUtil.getUserDetails();
		String response = retrieveUserServiceImpl.retrieveNOldestUsers(1);
		assertEquals(mockedResponseBodyStr, response);
	}

	@Test
	public void getOldestUsersTestException() throws Exception {
		String mockedResponseBodyStr = RetrieveUsersUtil.getUserDetailsEmpty();
		URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(gitHubApiEndpoint)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_QUALIFIER, githubqualifierValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_SORT, githubSortValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_ORDER, githubOrdervalue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PERPAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CID,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CIDV)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CS,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CSV)
				.build().encode().toUri();
		HttpHeaders header = new HttpHeaders();
		header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
		header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
		when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
				.thenThrow(RestClientException.class);
		String response = retrieveUserServiceImpl.retrieveNOldestUsers(1);
		assertEquals(mockedResponseBodyStr, response);
	}

	@Test
	public void getOldestUsersTestForArrayIndexOutofBounds() throws Exception {
		ResponseEntity<JsonNode> mockedResponse = RetrieveUsersUtil.getUserDetailsJSON();
		String mockedResponseBodyStr = RetrieveUsersUtil.getUserDetails();
		URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(gitHubApiEndpoint)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_QUALIFIER, githubqualifierValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_SORT, githubSortValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_ORDER, githubOrdervalue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PERPAGE, 2)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CID,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CIDV)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CS,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CSV)
				.build().encode().toUri();
		HttpHeaders header = new HttpHeaders();
		header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
		header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
		when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
				.thenReturn(mockedResponse);
		String response = retrieveUserServiceImpl.retrieveNOldestUsers(2);
		assertEquals(mockedResponseBodyStr, response);
	}

	@Test
	public void getOldestUsersTestForJsonMappingException() throws Exception {
		ResponseEntity<JsonNode> mockedResponse = RetrieveUsersUtil.getUserDetailsJSONNew();
		String mockedResponseBodyStr = "[]";
		URI gitAPIUrl = UriComponentsBuilder.fromHttpUrl(gitHubApiPrefix).path(gitHubApiEndpoint)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_QUALIFIER, githubqualifierValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_SORT, githubSortValue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_ORDER, githubOrdervalue)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PAGE, 1)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_PERPAGE, 2)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CID,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CIDV)
				.queryParam(RetrieveOldestUsersConstants.GITHUBAPIPARAM_CS,
						RetrieveOldestUsersConstants.GITHUBAPIPARAM_CSV)
				.build().encode().toUri();
		HttpHeaders header = new HttpHeaders();
		header.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON.toString());
		header.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON.toString());
		HttpEntity<HttpHeaders> requestEntity = new HttpEntity<>(header);
		when(restTemplate.exchange(gitAPIUrl, HttpMethod.GET, requestEntity, JsonNode.class))
				.thenReturn(mockedResponse);
		String response = retrieveUserServiceImpl.retrieveNOldestUsers(2);
		assertEquals(mockedResponseBodyStr, response);
	}

	@Test
	public void miscellaneousTests() {
		RetrieveOldestUsersConstants constants = new RetrieveOldestUsersConstants();
		RetrieveUsersUtil retrieveUsersUtilObj = new RetrieveUsersUtil();
		MicroServiceMain.main(new String[] {});
	}

}
