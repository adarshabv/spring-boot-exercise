package com.telstra.codechallenge.retrieveusers.util;

import org.springframework.http.ResponseEntity;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RetrieveUsersUtil {

	public static String getUserDetails() throws Exception {
		String jsonResponse = "[{\"id\":\"44\",\"login\":\"errfree\",\"html_url\":\"https://github.com/errfree\"}]";
		return jsonResponse;
	}

	public static ResponseEntity<JsonNode> getUserDetailsJSON() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = "{ \"items\": [ { \"login\": \"errfree\",\"id\": 44,\"html_url\": \"https://github.com/errfree\"}]}";
		JsonNode jsonNode = objectMapper.readTree(jsonResponse);
		return ResponseEntity.ok(jsonNode);
	}
	
	public static ResponseEntity<JsonNode> getUserDetailsJSONNew() throws Exception {
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonResponse = "{ \"items\":  { \"login\": \"errfree\",\"id\": 44,\"html_url\": \"https://github.com/errfree\"}}";
		JsonNode jsonNode = objectMapper.readTree(jsonResponse);
		return ResponseEntity.ok(jsonNode);
	}

	public static String getUserDetailsEmpty() throws Exception {
		String jsonResponse = "[]";
		return jsonResponse;
	}

}
